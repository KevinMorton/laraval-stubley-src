import Home from "./components/Home";
import ExampleComponent from "./components/ExampleComponent";
import Register from "./components/Register";
import Login from "./components/Login";
import Dashboard from "./components/Dashboard";


export const routes = [
    {
        name: 'home',
        path: '/',
        component: Home
    },
    {
        name: 'add',
        path: '/example',
        component: ExampleComponent
    },
    {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            auth: false
        }
    }, {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {auth: false},
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {auth: true}
    }


];
