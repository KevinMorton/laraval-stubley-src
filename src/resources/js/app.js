/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');


import App from './App.vue';
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import Home from "./components/Home";
import ExampleComponent from "./components/ExampleComponent";
import Register from "./components/Register";
import Login from "./components/Login";
import Dashboard from "./components/Dashboard";
import VueAuth from '@websanova/vue-auth'
import AuthBearer from '@websanova/vue-auth'
import {routes} from './routes';

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
axios.defaults.baseURL = 'http://localhost:8000/api';
const router = new VueRouter({
    routes: [{
        path: '/', name: 'home',
        component: Home
    }, {
        path: '/register',
        name: 'register',
        component: Register, meta: {
            auth: false
        }
    }, {
        path: '/login', name: 'login',
        component: Login, meta: {auth: false}
    }, {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {auth: true}
    }]
});
Vue.router = router;
Vue.use(VueAuth, {
    auth: AuthBearer,
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
});
App.router = Vue.router;

new Vue(App).$mount('#app');
