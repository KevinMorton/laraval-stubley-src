FROM php:7.4-cli

WORKDIR /var/www/html

RUN apt-get update && apt-get install -y \
		libfreetype6-dev \
		libjpeg62-turbo-dev \
		libpng-dev \
		libwebp-dev \
		libxpm-dev \
		libgif-dev \
		libldap2-dev \
		libgmp-dev \
		libzip-dev \
		libcurl3-dev \
		netcat  \
	&& docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg --with-webp -with-xpm \
	&& docker-php-ext-install -j$(nproc) gd

RUN docker-php-ext-install pdo pdo_mysql zip gmp exif json curl

